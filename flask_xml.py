# -*- coding: utf-8 -*-
from flask import Flask, render_template, make_response, redirect
import requests

url = 'http://api.metrazhi.site/objects?entity=1&perPage=1000&page=1'
response_json = requests.get(url).json()
values = response_json.get('data')
app = Flask(__name__)


@app.route('/')
def redirect_to_xml():
    return redirect("/feed")


@app.route('/feed')
def sitemap():
    template = render_template('base.xml', values=values)
    make_resp = make_response(template)
    make_resp.headers['Content-Type'] = 'application/xml'
    return make_resp


if __name__ == "__main__":
    app.run(host='0.0.0.0')
